;;; atom-tests.el --- Tests for the atom.el library  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Frédéric Perrin

;; Author: Frédéric Perrin <frederic.perrin@resel.fr>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(normal-top-level-add-to-load-path (list default-directory))
(require 'atom)


(ert-deftest text-feed ()
  (let* ((user-full-name "John Smith")
	 (user-mail-address "john.smith@example.org")
	 (now (current-time))
	 (my-atom-feed (atom-create "My feed" "http://example.org"
				    (list :updated now))))
    ;; A simple, text-only entry
    (atom-add-text-entry
     my-atom-feed
     "Hello world"
     "http://example.org/hello"
     "Hello the world!"
     (list :updated now))

    ;; <?xml version="1.0" encoding="utf-8"?>
    ;; <feed xmlns="http://www.w3.org/2005/Atom">
    ;; <title>My feed</title><link href="http://example.org"/><author>
    ;;   <name>John Smith</name>
    ;;   <email>john.smith@example.org</email>
    ;; </author><updated>2024-03-13T21:55:38+00:00</updated><id>http://example.org</id><entry>
    ;;   <title>Hello world</title>
    ;;   <link href="http://example.org/hello"/>
    ;;   <id>http://example.org/hello</id>
    ;;   <updated>2024-03-13T21:54:14+00:00</updated>
    ;;   <content>Hello the world!</content>
    ;; </entry>
    ;; </feed>

    (with-temp-buffer
      (atom-print my-atom-feed)
      (let ((expected-strings
	     (list
	      "<title>My feed</title>"
	      "<name>John Smith</name>"
	      "<email>john.smith@example.org</email>"
	      "<link href=\"http://example.org\"/>"
	      "<entry>[[:space:]]+<title>Hello world</title>"
	      "<link href=\"http://example.org/hello\"/>"
	      "<id>http://example.org/hello</id>"
	      "<content>Hello the world!</content>")))
	(dolist (exp-string expected-strings)
	  (goto-char (point-min))
	  (should (re-search-forward exp-string))))
      (goto-char (point-min))
      ;; there will be two updated elements, for the feed and the entry
      (re-search-forward "updated>\\(.*\\)</updated>")
      (let* ((updated-string (match-string 1))
	     (updated-time (atom-parse-time updated-string)))
	(should (equal updated-time (seq-take now 2))))
      (re-search-forward "updated>\\(.*\\)</updated>")
      (let* ((updated-string (match-string 1))
	     (updated-time (atom-parse-time updated-string)))
	(should (equal updated-time (seq-take now 2)))))))

(ert-deftest html-xhtml-feed ()
  (let ((my-atom-feed (atom-create "My feed" "http://example.org")))

    (atom-add-text-entry
     my-atom-feed
     "A text entry"
     "http://example.org/text"
     "Some text only")
    (atom-add-html-entry
     my-atom-feed
     "An HTML entry"
     "http://example.org/html"
     "<p>One can also use <acronym>HTML</acronym> in the entries.</p>")
    (atom-add-xhtml-entry
     my-atom-feed
     "A xHTML entry"
     "http://example.org/xhtml"
     "<p>One can also use <acronym>xHTML</acronym> in the entries.</p>")

    ;; only check that we can print the feed...
    (atom-print my-atom-feed)
    (atom-print-as-rss my-atom-feed)))

(ert-deftest atom-opt-elements ()
  (let ((my-atom-feed (atom-create "My Feed" "http://example.org"
				   (list :subtitle "Feed subtitle"
					 :self "http://example.org/feed.atom"
					 :id "urn:example-id:1"
					 :author (list "Author name" "Author@example.org")
					 :updated (atom-parse-time "2024-03-23T01:02:03+04:00")))))
    (atom-add-text-entry
     my-atom-feed
     "A text entry"
     "http://example.org/text"
     "Some text"
     (list :updated (atom-parse-time "2024-03-23T01:02:04+0400")
	   :published (atom-parse-time "2024-03-23T01:02:04+0400")
	   :summary "Summary"))
    (atom-add-html-entry
     my-atom-feed
     "A HTLM entry"
     "http://example.org/html"
     "<p>Some text</p>"
     (list :updated (atom-parse-time "2024-03-23T01:02:05+04:00")
	   :summary "<p>summary...</p>"))
    (atom-add-xhtml-entry
     my-atom-feed
     "A XHTML entry"
     "http://example.org/xhtml"
     "<p>Some text</p>"
     (list :updated (atom-parse-time "2024-03-23T01:02:06+04:00")
	   :summary "<p>summary...</p>"))

    (atom-print my-atom-feed)
    (atom-print-as-rss my-atom-feed "http://example.org/feed.rss")))

(provide 'atom-tests)
;;; atom-tests.el ends here
