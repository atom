;;; atom.el --- Create an Atom feed  -*- lexical-binding: t -*-

;; Copyright (C) 2011  Frédéric Perrin

;; Author: Frédéric Perrin <frederic.perrin@resel.fr>
;; Keywords: www, hypermedia, atom, rss

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a library for creating an Atom feed from a Lisp program.
;; The normal usage is to create a feed with `atom-create', giving it
;; a title and a Web address. Once the feed has been created, entries
;; may be added to the feed, by specifying (at the minimum) a title, a
;; permanent link and the content of the entry. Text-only, HTML and
;; XHTML entries are supported.

;; It is possible to produce both Atom and RSS feeds.

;; A typical usage would look like this:

;; (let ((my-atom-feed (atom-create "My feed" "http://example.org")))
;;   ; A simple, text-only entry
;;   (atom-add-text-entry
;;    my-atom-feed
;;    "Hello world"
;;    "http://example.org/hello"
;;    "Hello the world!")
;;
;;   (atom-add-xhtml-entry
;;    my-atom-feed
;;    "An XHTML example"
;;    "http://example.org/html-example"
;;    "<p>One can also use <acronym>XHTML</acronym> in the entries.</p>")
;;
;;   (atom-print my-atom-feed)
;;   ;; If you prefer RSS feeds:
;;   (atom-print-as-rss my-atom-feed))

;; Full documentation is available at <http://tar-jx.bz/code/atom.html>.
;; See atom-tests.el for usage examples.

;;; Code:

(require 'xml)
(require 'url-parse)

(defun atom-create (title link &optional props)
  "Create a new atom structure.

TITLE is the title for the feed, a short, text-only, human
readable string.

LINK is the URL of a page responible for the content of this
feed.

PROPS is an optional plist with the following properties:

- :subtitle is a subtitle for the feed; it can be a bit longer than
  TITLE, maybe a paragraph long.

- :self is the canonical URL to this feed. If missing, the resulting
  feed is non-conforming.

- :id is a unique identifier for this feed. If not given, it
  defaults to :self.

- :author is the author of the feed. See `atom-massage-author' for
the possible ways to specify it. In particular, nil uses
variable `user-full-name' and `user-mail-address'.

- :updated is the date the feed was last updated. If not given,
`(current-time)' is used."
  (let ((atom-feed (list (list 'title nil title))))
    (atom-modify-entry atom-feed 'link `(((href . ,link))))
    (atom-modify-entry atom-feed 'author (atom-massage-author (plist-get props :author)))
    (if (plist-member props :subtitle)
	(atom-modify-entry atom-feed 'subtitle (plist-get props :subtitle)))
    (if (plist-member props :self)
	(atom-modify-entry atom-feed 'link
			   `(((href . ,(plist-get props :self)) (rel . "self")
			      (type . "application/atom+xml")))))
    (atom-modify-entry atom-feed 'updated (atom-format-time (plist-get props :updated)))
    (atom-modify-entry atom-feed 'id (or (plist-get props :id) (plist-get props :self) link))
    atom-feed))

(defun atom-push-entry (atom entry)
  "Add the entry ENTRY to the feed ATOM."
  (nconc atom (list `(entry nil ,@entry))))

(defun atom-modify-entry (entry name val)
  "Set the NAME element of ENTRY to VAL."
  (let ((elem (if (stringp val)
		  (list name nil val)
		(cons name val))))
    (nconc entry (list elem))))

(defun atom-add-entry (atom title link content &optional props)
  "Add an entry to the atom flux ATOM.

Return the newly adde dentry.

TITLE is a short, text-only, human readable string.

LINK is a permanent link for this entry. For a given entry, LINK
may change between successive generations of the atom feed.

CONTENT is the content of the entry; use `atom-add-html-entry'
or `atom-add-xhtml-entry' when CONTENT is not text-only.

PROPS is an optional plist with the following properties:

- :summary, if is not given, the entry will not contain any summary.

- :updated defaults to `(current-time)'.

- :published, if given, is the earliest availability of the
  entry. It is optional, and shouldn't change even if the entry
  content (etc.) updated after the initial publication.

- :id is a unique ID for the entry; defaulting to LINK. RFC4287
  has specific requirements about valid IRI that may be used,
  which this library does not try to enforce."
  (let ((entry (list (list 'title nil title))))
    (atom-modify-entry entry 'link  (list (list (cons 'href link))))
    (atom-modify-entry entry 'id (or (plist-get props :id) link))
    (atom-modify-entry entry 'updated (atom-format-time (plist-get props :updated)))
    (if (plist-member props :published)
	(atom-modify-entry entry 'published (atom-format-time (plist-get props :published))))
    (if (plist-member props :summary)
	(atom-modify-entry entry 'summary (plist-get props :summary)))
    (atom-modify-entry entry 'content content)
    (atom-push-entry atom entry)
    entry))

(defalias 'atom-add-text-entry 'atom-add-entry
  "Add an entry to ATOM, with a textual content. See
`atom-add-entry' for details.")

(defun atom-add-html-entry (atom title link content &optional props)
  "Add an entry to ATOM, with some HTML content.

TITLE, LINK, PROPS as in `atom-add-entry'. CONTENT should be a string
enconding a valid HTML fragment. See `atom-add-entry' for
additional details."
  (if (plist-member props :summary)
      (plist-put props :summary (atom-massage-html (plist-get props :summary))))
  (atom-add-entry atom title link (atom-massage-html content) props))

(defun atom-add-xhtml-entry (atom title link content &optional props)
  "Add an entry to ATOM, with some XHTML content.

TITLE, LINK, PROPS as in `atom-add-entry'. CONTENT may be given
either as a string, or as an XML tree, of a valid XHTML fragment.
See `atom-add-entry' for additional details."
  (if (plist-member props :summary)
      (plist-put props :summary (atom-massage-xhtml (plist-get props :summary))))
  (atom-add-entry atom title link (atom-massage-xhtml content) props))

(defvar atom-xml-declaration "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")

(defun atom-print (atom)
  "Print the Atom feed ATOM in the current buffer."
  (insert atom-xml-declaration)
  (insert "<feed xmlns=\"http://www.w3.org/2005/Atom\">\n")
  (xml-print atom)
  (insert "\n</feed>"))

(defun atom-write-file (atom filename)
  "Writes the feed ATOM to FILENAME."
  (with-temp-buffer
    (atom-print atom)
    (write-file filename)))


(defun atom-to-rss (atom &optional rss-self)
  "Translate Atom feed ATOM into an RSS one, returning the translation.

If RSS-SELF is given, it is used as self link of the RSS feed.

Some information may be lost or approximated."
  (let ((rss (list (assoc 'title atom))))
    (if rss-self
	(atom-modify-entry rss 'atom:link
			   `(((href . ,rss-self) (rel . "self")
			      (type . "application/atom+xml")))))
    (atom-to-rss-translator atom rss '((subtitle . description)
				       (updated . pubDate)
				       (link . link)))
    (atom-to-rss-modify-time rss)
    (atom-to-rss-modify-link rss)
    (dolist (entry (xml-get-children atom 'entry))
      (push (atom-to-rss-item entry) rss))
    (reverse rss)))

(defun atom-to-rss-item (entry)
  "Translates the Atom entry ENTRY into an RSS item."
  (let ((item (list (assoc 'title entry))))
    (atom-to-rss-translator
     (xml-node-children entry) item
     '((id . guid) (content . description) (updated . pubDate) (link . link)))
    (atom-to-rss-modify-time item)
    (atom-to-rss-modify-link item)
    (let ((guid (assoc 'guid item))
	  (descr (assoc 'description item)))
      (if guid
	  (setcar (cdr guid) (list (cons 'isPermaLink "false"))))
      (if (and descr
	       (equal (xml-get-attribute descr 'type) "xhtml"))
	  (setcar (cddr descr) (xml-node-as-text descr)))
      (setcar (cdr descr) nil))
    `(item nil ,@item)))

(defun atom-to-rss-translator (source target translations)
  (dolist (translation translations)
    (let* ((from (car translation))
	   (to (cdr translation))
	   (data (copy-tree (cdr (assoc from source)))))
      (when data
	(atom-modify-entry target to data)))))

(defun atom-to-rss-modify-link (entry)
  (let* ((link (assoc 'link entry))
	 (link-addr (xml-get-attribute-or-nil link 'href)))
    (when link
      (setcar (cdr link) nil)
      (setcdr (cdr link) (cons link-addr nil)))))

(defun atom-print-as-rss (atom &optional rss-self)
  "Convert Atom feed ATOM to RSS in the current buffer.

If RSS-SELF is given, it is used as self link of the RSS feed."
  (let ((rss (atom-to-rss atom rss-self)))
    (insert atom-xml-declaration)
    ;; xmlns:atom included in order to allow the atom:link rel=self element
    (insert "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n")
    (insert "  <channel>\n")
    (xml-print rss "    ")
    (insert "\n  </channel>\n")
    (insert "</rss>")))

(defun atom-to-rss-time (time)
  "Translate TIME from the format used by Atom into the format used by RSS.

TIME is a string."
  (let ((system-time-locale "C"))
    (format-time-string "%a, %d %b %Y %T %z" (atom-parse-time time))))

(defun atom-to-rss-modify-time (entry)
  "Modify ENTRY, changing the format of the `pubDate' in it."
  (let ((pubDate (assoc 'pubDate entry)))
    (setcar (cddr pubDate)
	    (atom-to-rss-time (car (xml-node-children pubDate))))))

(defun atom-to-rss-write-file (atom filename &optional rss-self)
  "Save ATOM as a RSS feed into FILENAME.

If RSS-SELF is given, it is used as self link of the RSS feed."
  (with-temp-buffer
    (atom-print-as-rss atom rss-self)
    (write-file filename)))


(defvar atom-time-format-string "%Y-%m-%dT%T%z"
  "The format for string representation of dates.")

(defvar atom-xhtml-namespace "http://www.w3.org/1999/xhtml")

(defun atom-format-time (&optional time)
  "Format time value TIME according to RFC3339."
  ;; The time zone must be specified in numeric form, but with a colon between
  ;; the hour and minute parts.
  (replace-regexp-in-string
   "\\(..\\)$" ":\\1"
   (format-time-string atom-time-format-string time)))

(defun atom-parse-time (&optional time)
  "Parse string TIME as specified in RFC3339 into Emacs's native format."
  ;; Same remark as in `atom-format-time': RFC3339 wants a colon between hour
  ;; and minute parts of the timezome, so remove it before `date-to-time'.
  (date-to-time (replace-regexp-in-string ":\\(..\\)$" "\\1" time)))

(defun atom-massage-html (content)
  "Massage CONTENT so it can be used as an HTML fragment in an Atom feed.

CONTENT must be a string."
  (list '((type . "html")) content))

(defun atom-string-to-xml (string)
  "Convert STRING into a Lisp structure as used by `xml.el'."
  (require 'xml-xhtml-entities)
  (let ((xml-entity-alist xml-xhtml-entities)
	(xml-validating-parser t))
    (with-temp-buffer
      (insert "<div xmlns=\"" atom-xhtml-namespace "\">")
      (insert string)
      (insert "</div>")
      ;; `xml-parse-region' returns a list of elements, even though it
      ;; requires an only root node. We are only interested in the first
      ;; one, the DIV we just inserted.
      (car (xml-parse-region (point-min) (point-max))))))

(defun atom-massage-xhtml (content)
  "Massage CONTENT so it can be used as an XHTML fragment in an Atom feed."
  (list '((type . "xhtml"))
	(or (and (stringp content)
		 (atom-string-to-xml content))
	    `(div ((xmlns . ,atom-xhtml-namespace)) ,@content))))

(defun atom-massage-author (author)
  "Return an XML node representing the author. AUTHOR can be:
- nil, in which case variables `user-full-name' and `user-mail-address'
  are used;
- a single string, the full name of the author; no email address
  will be included;
- a list with two elements, the full name and the email address
  of the author;
- something else, assumed to be a complete `atomPersonConstruct'."
  `(nil ,@(cond
	   ((null author) `((name nil ,user-full-name)
			    (email nil ,user-mail-address)))
	   ((stringp author) `((name nil ,author)))
	   ((= 2 (length author)) `((name nil ,(car author))
				    (email nil ,(cadr author))))
	   (t `(author nil ,author)))))

(defun atom-xhtml-convert-links (node base)
  "Make all links in NODE (a fragment of an XHTML document)
absolute, in the context of BASE, an URL."
  (dolist (attr-name (list 'href 'src))
    (let ((attr (assoc attr-name (xml-node-attributes node))))
      (when attr (setcdr attr (url-canonalize (cdr attr) base)))))
  (dolist (child (xml-node-children node))
    (when (listp child) (atom-xhtml-convert-links child base))))


;;; Functions that should probably not be there

(defun url-canonalize (address base)
  "Make ADDRESS an absolute URL, taking it in the BASE context."
  ;; I feel such a function should exist in `url-parse'. Did I miss it?
  (let ((url-base (url-generic-parse-url base))
	(url-address (url-generic-parse-url address)))
    (if (url-host url-address)
	address
      (setf (url-filename url-base)
	    (expand-file-name address
			      (file-name-directory (url-filename url-base))))
      (url-recreate-url url-base))))

(defun xml-node-as-text (node)
  "Return a string representing NODE, an XML structure."
  (with-temp-buffer
    (xml-print (xml-node-children node))
    (buffer-string)))

(provide 'atom)
;;; atom.el ends here
